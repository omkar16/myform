import React from 'react'
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  navigation
} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



export default class Login extends React.Component {
    signupNav = () => {
      this.props.navigation.push('Drawer' );
          }
        
    render() {

    
        return (
            <View style={styles.container}>
    <ImageBackground
      source={require('../assest/bac.png')}
      style={styles.image}>
          <Text style={{alignSelf:'center',justifyContent:'center',paddingTop:200,fontSize:16,fontWeight:'bold'}}>Welcome,MyFormApp..</Text>
      <View style={{marginTop: 420}}>

     
   

        <TouchableOpacity 
         onPress={this.signupNav}
         style={[styles.button,styles.margins]}>
          <Text style={styles.fontText}>Get Started</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  </View>
        )
    }
}







const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  image: {
    flex: 1,
    resizeMode: 'stretch',
   

   
  },
  text: {
    color: 'grey',
    fontSize: 30,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 50,
    height: 50,
    width: 200,
    borderColor: 'black',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignSelf: 'center',
    margin: 20,
    elevation: 20,
  },
  fontText: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'sans-serif',
    fontWeight: 'bold',
  },
  margins:{
    marginBottom:160
  }
})


