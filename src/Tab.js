import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Email from './Email'
import Login from './Login'
import Mobile from './mobile'

const Tab = createMaterialTopTabNavigator();

export default function App() {
  return (
    <NavigationContainer 
    independent={true} 
    shifting ='true'
    >
      <Tab.Navigator
        
             shifting ="true"
        tabBarOptions ={{
          pressColor:'#FFA500',
          activeTintColor:'#FFA500',
          style:{
            borderBottomColor:'#FFA500',
             elevation:0
          },
          indicatorStyle:{
            backgroundColor:'#FFA500',
            
          },
          labelStyle:{
            fontWeight:'bold',
            
          }
          
           
        }  
      } >
        <Tab.Screen 
          
        
        name=" Using E-mail" component={Email} />
        <Tab.Screen
           
        name="Using Mobile Number" component={Mobile} />
       
      </Tab.Navigator>
    </NavigationContainer>
  );
}
