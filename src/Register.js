






 import React, { Component } from 'react'
 import { Text, View ,StyleSheet,ImageBackground,TouchableOpacity} from 'react-native'
 
 export default class Register extends Component {
  signupNav = () => {
    this.props.navigation.push('NavTab')
      }
  
   render() {
     return (
      <View style={styles.container}>
      <ImageBackground source={require('../assest/home.png')} style={styles.image}>
        <View style={{marginTop:450}}>
      <TouchableOpacity  
       onPress= {this.signupNav }
            style={styles.button}>
            <Text style={{fontSize: 20, alignSelf: 'center'}}>Registration</Text>
          </TouchableOpacity>
  
          <TouchableOpacity
         
            style={styles.button1}>
            <Text style={{fontSize: 20, alignSelf: 'center'}}>Login</Text>
          </TouchableOpacity>
          </View>
      </ImageBackground>
    </View>
     )
   }
 }
 



const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "grey",
    fontSize: 30,
    fontWeight: "bold"
  },
  button1:{
    borderRadius: 50,
    height: 50,
    width: 200,
    borderColor: 'black',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignSelf: 'center',

  },
  button:{
    borderRadius: 50,
    height: 50,
    width: 200,
    borderColor: 'black',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignSelf: 'center',
    margin:20

  
  }
});

