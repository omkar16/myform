import React, {Component} from 'react'
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  TouchableHighlight
} from 'react-native'
import Radio from './radio'
import Modal from "react-native-simple-modal";

class Mobile extends React.Component {
  state = { open: false };
 
  modalDidOpen = () => console.log("Modal did open.");
 
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };

  register=()=>{
    this.props.navigation.push('Enable')
  }
 

  moveUp = () => this.setState({ offset: -100 });
  openModal = () => this.setState({ open: true ,offset: -100  });
 
  closeModal = () => this.setState({ open: false });

  render (props) {
    return (
      <View  
       style={styles.wrapper}>
         
          
        <TextInput
          style={styles.input}
          keyboardType='email-address'
          placeholder='User Name'
          autoCapitalize='none'
          onChangeText={email => {
            this.setState({email}, () => {})
          }}
        />

    < TextInput
          style={styles.input}
          keyboardType='number-pad'
          placeholder=' Mobile Number'
          autoCapitalize='none'
          onChangeText={email => {
            this.setState({email}, () => {})
          }}
        />
   
        <TextInput
          style={styles.input}
          keyboardType='default'
          secureTextEntry={true}
          placeholder='Password'
          autoCapitalize='none'
          onChangeText={password => this.setState({password})}
        />

        <TextInput
          style={styles.input}
          keyboardType='default'
          secureTextEntry={true}
          placeholder='  Confirm  Password'
          autoCapitalize='none'
          onChangeText={password => this.setState({password})}
        />
               <Radio {...props}/>

               <TouchableOpacity 
   style={styles.button}
   onPress={this.openModal}
      >
          <Text
            style={styles.fontText}>
            Register
          </Text>
        </TouchableOpacity>
        
        
        <Modal
          offset={this.state.offset}
          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center", }}
        >
        <View style={{ alignItems: "center",height:250,backgroundColor:'white',borderWidth:1,borderColor:'#FFA500' }}>
        <Image
                    source={require('../assest/call.png')}
                    style={styles.image}></Image>
            <Text style={{ fontSize: 20, justifyContent:'center',alignSelf:'center',fontFamily:'monospace',marginTop:5}}>Verify Your Mobile Number</Text>
            <Text style={{ fontSize: 16, alignSelf:'flex-start',margin:10,color:'grey'}}> Mobile Number</Text>
    <View style={{flexDirection:'row',paddingRight:50}}>
            <View style={styles.otp1}>
              <Text style={{alignSelf:'center'}}> +91
              </Text></View>
            <View style={styles.otp}/>
            </View>
            
             <View style={{flexDirection:'row'}}>
               <TouchableOpacity 
                 onPress={this.closeModal}
               style={styles.cancel}>
                 <Text style={{  justifyContent:'center',alignSelf:'center',fontFamily:'monospace'}}>
                  Cancel
                 </Text>
               </TouchableOpacity>
               <TouchableOpacity
                 onPress={this.register}
               style={styles.submit}>
                <Text style={{ fontSize: 18, justifyContent:'center',alignSelf:'center',fontFamily:'monospace',color:'white'}}>
                  Submit
                </Text>
</TouchableOpacity>
             </View>
           
          </View>
        </Modal>
      
      </View>
    )
  }
}
const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flex: 1,
    padding: 20,
    backgroundColor:'white',
    
  },
  broder: {
    borderWidth: 1,
    borderColor: '#D3D3D3',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  input: {
    borderWidth: 1,

    padding: 10,
    borderColor: '#D3D3D3',
    borderRadius: 20,
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical:10,
    padding:13,
    backgroundColor: '#eeeeee',
  },
    button: {
    borderRadius: 50,
    height: 50,
    width: 250,
    borderColor: 'black',
    backgroundColor: '#FFA500',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    
  },
  fontText: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'sans-serif',
    fontWeight: 'bold',
    color:'white'
  },
   image: {
    width: 60,
    height: 60,
    marginTop:5
  
    
},
otp:{
  borderRadius: 10,
  height: 40,
  width: 180,
  borderColor: 'black',
  borderWidth:1,
  
  margin:5
 
},
otp1:{
  borderRadius: 10,
  height: 40,
  width: 50,
  borderColor: 'black',
  borderWidth:1,
  justifyContent: 'center',
  alignSelf: 'center',
 
},
cancel:{
  borderRadius: 10,
    height: 40,
    width: 120,
    borderColor: 'black',
    backgroundColor: '#eeeeee',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 10,
    marginRight:10
    
},
submit:{
  borderRadius: 10,
    height: 40,
    width: 120,
    borderColor: 'black',
    backgroundColor: '#FFA500',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 10,
    marginLeft:10
    
}
})

 export default Mobile



// import React from "react";
// import { Text, TouchableOpacity, View,Image } from "react-native";
// import Modal from "react-native-simple-modal";
 
// export default class App extends React.Component {
//   state = { open: false };
 
//   modalDidOpen = () => console.log("Modal did open.");
 
//   modalDidClose = () => {
//     this.setState({ open: false });
//     console.log("Modal did close.");
//   };
 

 
//   openModal = () => this.setState({ open: true });
 
//   closeModal = () => this.setState({ open: false });
 
//   render() {
//     return (
//       <View style={{ flex: 1, justifyContent: "center", alignItems: "center" ,backgroundColor:'white'}}>
//         <TouchableOpacity onPress={this.openModal}>
//           <Text>Open modal</Text>
//         </TouchableOpacity>
//         <Modal
//           offset={this.state.offset}
//           open={this.state.open}
//           modalDidOpen={this.modalDidOpen}
//           modalDidClose={this.modalDidClose}
//           style={{ alignItems: "center",backgroundColor:'white' }}
//         >
//           <View style={{ alignItems: "center" ,padding:20,backgroundColor:'white',borderWidth:1}}>
//           <Image
//                    source={require('../assest/call.png')}
//                   style={{width:40,height:40,backgroundColor:'blue'}}/>
//             <Text style={{ fontSize: 20, marginBottom: 10 }}>Hello world!</Text>
//             <TouchableOpacity style={{ margin: 5 }} onPress={this.moveUp}>
//               <Text>Move modal up</Text>
//             </TouchableOpacity>
        
           
//           </View>
//         </Modal>
//       </View>
//     );
//   }
// }