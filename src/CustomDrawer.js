import React from 'react'
import { StyleSheet, Text, View,ImageBackground } from 'react-native'
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
  } from '@react-navigation/drawer';

export default function CustomDrawer(props) {
    return (
        
        <DrawerContentScrollView {...props} >
      
      <ImageBackground style={{ height: 980, width: 400 }} source={require('../assest/ba.png')}   >
          <View style={{paddingTop:150}}>
      <DrawerItemList  activeBackgroundColor='orange' activeTintColor='white'  {...props} />
      </View>
</ImageBackground>
    </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({})
