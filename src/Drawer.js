import * as React from 'react';
import { View, Text, Button, Image, ImageBackground } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Activity from './Activity'

import Profile from './Profile'
import Service from './Service'
import Terms from './Terms'
import Logout from './Logout'
import Icon from 'react-native-vector-icons/MaterialIcons';
import CustomDrawer from './CustomDrawer'
function Home({ navigation }) {
  return (
    <View style={{ flex: 1, }}>
      <ImageBackground style={{ height: 1000, width: 400 }} source={require('../assest/ba.png')}   >

        <TouchableOpacity onPress={() => navigation.openDrawer()}>
        <Icon name='menu' size={40} color='white' />

        </TouchableOpacity>
      </ImageBackground>



    </View>
  );
}








const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator 
      initialRouteName='Home'
      style={{paddingTop:200}}
      hideStatusBar={true}
      drawerStyle={{
        backgroundColor: '#FFA500',
        width: 240,
      
         
      }}
      
    drawerContent={props => <CustomDrawer{...props} />} 
    
>
      <Drawer.Screen   name="Home" component={Home} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Service" component={Service} />
      <Drawer.Screen name="Terms" component={Terms} />
      <Drawer.Screen name="Activity" component={Activity} />
      <Drawer.Screen name="Logout" component={Logout} />
    </Drawer.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer independent={true}  >
      <MyDrawer />
    </NavigationContainer>
  );
}
