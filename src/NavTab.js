import * as React from 'react';
import { View, Text, Button, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TabNav from './TabNav'
import Tab from './Tab'
import Register from './Register'
import Enable from './Enable'
import Drawer from './Drawer'
import Email from './Email'
const Stack = createStackNavigator();






function Root1() {

  

 
  return (
    
          <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Tab}
            options={{
              headerTitle: props => <TabNav {...props} />,
             
              headerStyle:{
                height:120,
                 elevation:0,
                  
              }
            }}
          />
           <Stack.Screen
            name="Tab"
            component={Tab}
            
           
          />
        </Stack.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer  independent ={true}>
     <Stack.Navigator
      initialRouteName="Root1"
      independent ={true}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        cardOverlayEnabled: true,
        
      }}
      
    >
      <Stack.Screen name="Root1" component={Root1} />

      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Email" component={Email} />
      <Stack.Screen name="Enable" component={Enable} />
      <Stack.Screen name="Drawer" component={Drawer} />
      
         </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
