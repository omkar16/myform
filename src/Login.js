import React from 'react'
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  navigation
} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



export default class Login extends React.Component {
    signupNav = () => {
      this.props.navigation.push( 'Register' );
          }
        
    render() {

    
        return (
            <View style={styles.container}>
    <ImageBackground
      source={require('../assest/background.png')}
      style={styles.image}>
      <View style={{margin: 20}}>

      <TouchableOpacity 
        onPress={this.signupNav}
      style={styles.button}>
          <Text
            style={styles.fontText}>
            User
          </Text>
        </TouchableOpacity>
        <TouchableOpacity 
           onPress={this.signupNav}
        style={styles.button}>
          <Text
            style={styles.fontText}>
            Service Provider
          </Text>
        </TouchableOpacity>

        <TouchableOpacity 
         onPress={this.signupNav}
         style={[styles.button,styles.margins]}>
          <Text style={styles.fontText}>Both</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  </View>
        )
    }
}







const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'stretch',

    justifyContent: 'center',
  },
  text: {
    color: 'grey',
    fontSize: 30,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 50,
    height: 50,
    width: 200,
    borderColor: 'black',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignSelf: 'center',
    margin: 20,
    elevation: 20,
  },
  fontText: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'sans-serif',
    fontWeight: 'bold',
  },
  margins:{
    marginBottom:160
  }
})


