import React, {Component} from 'react'
import {Text, StyleSheet, View, ImageBackground} from 'react-native'
import Tab from './Tab'
export default class App extends Component {
  render () {
    return (
     
        <View style={{justifyContent:'center',alignSelf:'center',paddingBottom:100,paddingTop:100}}>
        <ImageBackground
          source={require('../assest/tog.png')}
          style={styles.image}></ImageBackground>
        <Text
          style={{
         
            fontSize: 20,
            color: '#808088',
            alignSelf: 'center',
            fontWeight:'bold',
            padding:10
          }}>
          Create Account
        </Text>
    
       
        </View>
        
      
    )
  }
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'stretch',
    width: 200,
    height: 50,
  
    
  },
  broder: {
    borderWidth: 1,
    borderColor: '#FFA500',
    fontSize: 20,
    textAlign: 'center',
    marginHorizontal: 40,
  },
})



