import React, {Component} from 'react'
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native'
import Radio from './radio'

class Email extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      apiCallInProgress: false,
      phone: '',
      email: '',
      loader_visibility: false,
      valid: false,
      error: '',
    }
  }

  register=()=>{
    this.props.navigation.push('Enable')
  }

  render (props) {
    return (
      <View style={styles.wrapper}>
        <TextInput
          style={styles.input}
          keyboardType='email-address'
          placeholder='User Name'
          autoCapitalize='none'
          onChangeText={email => {
            this.setState({email}, () => {})
          }}
        />

    < TextInput
          style={styles.input}
          keyboardType='email-address'
          placeholder='E-mail Id'
          autoCapitalize='none'
          onChangeText={email => {
            this.setState({email}, () => {})
          }}
        />

        <TextInput
          style={styles.input}
          keyboardType='default'
          secureTextEntry={true}
          placeholder='Password'
          autoCapitalize='none'
          onChangeText={password => this.setState({password})}
        />

        <TextInput
          style={styles.input}
          keyboardType='default'
          secureTextEntry={true}
          placeholder='  Confirm  Password'
          autoCapitalize='none'
          onChangeText={password => this.setState({password})}
        />
         <Radio {...props}/>
         <TouchableOpacity 
        onPress ={this.register}

      style={styles.button}>
          <Text
            style={styles.fontText}>
            Register
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flex: 1,
    padding: 20,
    backgroundColor:'white'
  },
  broder: {
    borderWidth: 1,
    borderColor: '#D3D3D3',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  input: {
    borderWidth: 1,
    margin: 5,
    padding: 10,
    borderColor: '#D3D3D3',
    borderRadius: 20,
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical:10,
    padding:13,
    backgroundColor: '#eeeeee',
  },
  button: {
    borderRadius: 50,
    height: 50,
    width: 250,
    borderColor: 'black',
    backgroundColor: '#FFA500',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    elevation: 2,
  },
  fontText: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'sans-serif',
    fontWeight: 'bold',
    color:'white'
  },
})

export default Email
