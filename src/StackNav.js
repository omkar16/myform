
import * as React from 'react';
import { View, Button, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Login from './Login'
import Register from './Register'
import TabNav from './TabNav'
import Email from './Email'
import NavTab from './NavTab'
import Tab from './Tab'
import Radio from './radio'
import Enable from './Enable'
import Drawer from './Drawer'
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';



const Stack = createStackNavigator();

function Root() {

  

 
  return (
    <Stack.Navigator
      initialRouteName="Login"
      independent ={true}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        cardOverlayEnabled: true,
        
      }}
      
    >
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="NavTab" component={NavTab} />
      <Stack.Screen name="Enable" component={Enable} />
      
      <Stack.Screen name="Email" component={Email} />
      <Stack.Screen name="Radio" component={Radio} />
    
  
         </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer independent={true}>
      <Root />
    </NavigationContainer>
  );
}




