import React, {Component} from 'react'
import {Text, StyleSheet, View, ImageBackground} from 'react-native'
import {Card} from 'react-native-shadow-cards'
export default class App extends Component {
  render () {
    return (
      <View>
        <Card>
          <ImageBackground
            style={{width: 400, height: 800}}
            source={{
              uri:
                'https://www.gettyimages.com/gi-resources/images/frontdoor/creative/PanoramicImagesRM/FD_image.jpg',
            }}>
            <Card style={styles.cardContainer}>
              <Text style={styles.title}>Diego</Text>
              <Text style={styles.subtitle}>Euphorbia Eritrea</Text>
            </Card>
            <View style={{flexDirection:'row'}}>
            <View>
              <Text
                style={{
                  color: 'white',
                  paddingLeft: 30,
                  fontSize: 30,
                  paddingTop: 30,
                }}>
                Plant care{' '}
              </Text>
              <Text style={{color: 'white', paddingLeft: 100, paddingTop: 30}}>
                Every 5 weeks{' '}
              </Text>
              <Text style={{color: 'white', paddingLeft: 100, paddingTop: 30}}>
                Planting{' '}
              </Text>
              <Text style={{color: 'white', paddingLeft: 100, paddingTop: 30}}>
                Minimum of 1 C{' '}
              </Text>
            </View>
            <View style={{paddingTop:40,overflow:'hidden',paddingLeft:40}}>
              
                <ImageBackground
                  style={{width: 400, height: 200,width:150,  borderBottomLeftRadius: 30,
                   
                    borderTopLeftRadius: 30,
                    overflow: 'hidden',
                  }}
                  source={{
                    uri:
                    "https://www.chambre237.com/wp-content/uploads/2017/09/Un-Photographe-professionnel-partage-ses-Secrets-pour-capturer-des-Photos-de-Paysage-parfaites-03.jpg"
                  }}>
                  
                </ImageBackground>
              
            </View>
            </View>
            <View>
              <Text
                style={{
                  color: 'white',
                  paddingLeft: 30,
                  fontSize: 30,
                  paddingTop: 30,
                }}>
                information
              </Text>
              <Text style={{color: 'white', paddingLeft: 30, padding: 10}}>
                What is Lorem Ipsum Lorem Ipsum is simply
              </Text>
              <Text style={{color: 'white', paddingLeft: 30}}>
                of the printing and typesetting industry Lorem
              </Text>
              <Text style={{color: 'white', paddingLeft: 30, padding: 10}}>
                been the industry's standard
              </Text>
            </View>
          </ImageBackground>
        </Card>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    padding: 20,
    width: 400,
    height: 300,
    //     //Below lines will help to set the border radius
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,

    overflow: 'hidden',
  },
  title: {
    fontSize: 30,
    paddingTop: 30,
  },
  subCardContainer: {
    padding: 20,
    width: 400,
    height: 300,
    //     //Below lines will help to set the border radius
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,

    overflow: 'hidden',
  },
})