import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default class radio extends Component {

    constructor (props) {
        super(props)
    
        this.state = {
           selected:false
        }
      }
      signupNav = () => {
        this.props.navigation.push('Modal')
          }
        
   

    changes =()=>{
        this.setState({
            selected:!this.state.selected
        })
    }      
    render(props) {
        return (
            <View style={{margin:20,marginTop:40,flexDirection:'row'}}>
            <TouchableOpacity 
            onPress ={this.changes}>
                {this.state.selected?(
                    <Image
                    source={require('../assest/right.png')}
                    style={styles.image}></Image>
                ):<TouchableOpacity style={styles.circle}/>
                }
            </TouchableOpacity>
            <View>
            <Text style={{paddingLeft:20}}>I Agree <Text style={{textDecorationLine:'underline',color:'#FFA500',textDecorationColor:'#FFA500',}}>Terms and Condition</Text>  of MyForm</Text>
           
           
        </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    circle: {
        height: 17,
        width: 17,
        borderRadius:4,
        borderWidth: 2,
        borderColor: 'black',
       
        overflow:'hidden'
        
    },
    image: {
        width: 17,
        height: 17,
        borderRadius: 2,
        borderWidth: 6,
        overflow:'hidden',
        
    },
    button: {
        borderRadius: 50,
        height: 50,
        width: 250,
        borderColor: 'black',
        backgroundColor: '#FFA500',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 30,
        elevation: 2,
      },
      fontText: {
        fontSize: 20,
        alignSelf: 'center',
        fontWeight: 'sans-serif',
        fontWeight: 'bold',
        color:'white'
      },
})




