import React, {Component} from 'react'
import {Text, StyleSheet, View, Button, ScrollView,ImageBackground,Image} from 'react-native'
import {Card} from 'react-native-shadow-cards'

export default class App extends Component {
  render () {
    return (
      <ScrollView>
        <Text style={{fontSize: 30,padding:30}}>My plants</Text>
        <View

        style={{paddingLeft: 40}}>

          <Card
            style={styles.cardContainer}

             >
                  <ImageBackground style={{width:400,height:200}} source={{uri: 'https://www.gettyimages.com/gi-resources/images/frontdoor/creative/PanoramicImagesRM/FD_image.jpg',}} >
            <Text style={styles.title}>Diego</Text>
            <Text style={styles.subtitle}>Euphorbia Eritrea</Text>
            </ImageBackground>
          </Card>

          <Card
            style={styles.cardContainer}
           >

<ImageBackground style={{width:400,height:200}} source={{uri: 'https://www.gettyimages.com/gi-resources/images/frontdoor/creative/PanoramicImagesRM/FD_image.jpg',}} >
            <Text style={styles.title}>Cassy</Text>
            <Text style={styles.subtitle}>Euphorbia Eritrea.</Text>
            </ImageBackground>
          </Card>

          <Card
            style={styles.cardContainer}
            source={{
              uri:
                'https://www.gettyimages.com/gi-resources/images/frontdoor/creative/PanoramicImagesRM/FD_image.jpg',
            }}>
               <ImageBackground style={{width:400,height:200}} source={{uri: 'https://www.gettyimages.com/gi-resources/images/frontdoor/creative/PanoramicImagesRM/FD_image.jpg',}} >
           <Text style={styles.title}>Luna</Text>
            <Text style={styles.subtitle}>Euphorbia Eritrea.</Text>
            </ImageBackground>
          </Card>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    width: 400,
    height: 200,
    //Below lines will help to set the border radius
    borderBottomLeftRadius: 30,
    margin: 15,
    borderTopLeftRadius: 30,
    overflow: 'hidden',

  },

  title: {
    fontSize: 20,
    color:'white',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 20,
  },
  subtitle: {
    color:'white',
    justifyContent: 'center',
    alignSelf: 'center',
  },
})






