import * as React from 'react';
import { View, Button, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import StackNav from './src/StackNav'
import NavTab from './src/NavTab'
import Login from './src/Login'
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';



const Stack = createStackNavigator();

function MyStack() {

  

 
  return (
    <Stack.Navigator
    independent ={true}
      initialRouteName="Login"
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        cardOverlayEnabled: true,
      
      }}
      
    >
    
      <Stack.Screen name="StackNav" component={StackNav} />
      <Stack.Screen name="NavTab" component={NavTab} />
     
         </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}




